/*
===============================================================================
 Name        : blink_m0.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "board.h"

#include "ciaaIO.h"

void MX_CORE_IRQHandler(void)
{
	LPC_CREG->M4TXEVENT = 0; 	/* ACK */

	ciaaToggleOutput(4);
}

int main(void) {
	int i;

#if defined (__USE_LPCOPEN)
#if !defined(NO_BOARD_LIB)
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
    // Set up and initialize all required blocks and
    // functions related to the board hardware
    Board_Init();
#endif
#endif

	NVIC_EnableIRQ(M4_IRQn);

    while(1)
    {
		__DSB(); /* Data Synchronization Barrier, asegura que se completan
		los accesos a memoria antes de ejecutar la próxima instrucción (necesario
		para usar SEV). */
		__SEV(); /* SendEvent -> IRQ to M4 core */
		for(i=0; i<0x3FFFFF; i++);
    }
}
