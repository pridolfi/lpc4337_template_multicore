/*
===============================================================================
 Name        : blink_m4.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#if defined (LPC43_MULTICORE_M0APP) | defined (LPC43_MULTICORE_M0SUB)
#include "cr_start_m0.h"
#endif

#include "ciaaIO.h"

void MX_CORE_IRQHandler(void)
{
	LPC_CREG->M0APPTXEVENT = 0; 	/* ACK */
	ciaaToggleOutput(3);
}

int main(void)
{
	int i;

#if defined (__USE_LPCOPEN)
#if !defined(NO_BOARD_LIB)
    // Read clock settings and update SystemCoreClock variable
    SystemCoreClockUpdate();
    // Set up and initialize all required blocks and
    // functions related to the board hardware
    Board_Init();
#endif
#endif

    ciaaIOInit();

    // Start M0APP slave processor
#if defined (LPC43_MULTICORE_M0APP)
    cr_start_m0(SLAVE_M0APP,&__core_m0app_START__);
#endif

	NVIC_EnableIRQ(M0APP_IRQn);

	while(1)
	{
		__DSB(); /* Data Synchronization Barrier, asegura que se completan
		los accesos a memoria antes de ejecutar la próxima instrucción (necesario
		para usar SEV). */
		__SEV(); /* SendEvent -> IRQ to M0 core */
		for(i=0; i<0x3FFFFF; i++);
	}

}
