export APPLICATION := $(notdir $(shell pwd))

PROJECTS := lpcopen_lpc4337_m4 lpcopen_lpc4337_m0 app_m0 app_m4

ROOT_PATH := $(shell pwd)

export SRC_PATH  := src
export INC_PATH  := inc
export OUT_PATH  := $(ROOT_PATH)/out

export OBJ_PATH_M4  := $(OUT_PATH)/obj_m4
export OBJ_PATH_M0  := $(OUT_PATH)/obj_m0

export SYMBOLS_M4 := -DDEBUG -DCORE_M4 -D__USE_LPCOPEN -D__LPC43XX__ -D__CODE_RED -DLPC43_MULTICORE_M0APP -D__MULTICORE_MASTER -D__MULTICORE_MASTER_SLAVE_M0APP
export CFLAGS_M4  := -Wall -ggdb3 -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -fdata-sections -ffunction-sections -c
export LFLAGS_M4  := -nostdlib -fno-builtin -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -Xlinker -Map=$(OUT_PATH)/$(APPLICATION)_m4.map -Wl,--gc-sections

export SYMBOLS_M0 := -DDEBUG -DCORE_M0 -D__USE_LPCOPEN -D__LPC43XX__ -D__CODE_RED -D__MULTICORE_M0APP -DCORE_M0APP
export CFLAGS_M0  := -Wall -ggdb3 -mcpu=cortex-m0 -mthumb -fdata-sections -ffunction-sections -c
export LFLAGS_M0  := -nostdlib -fno-builtin -mcpu=cortex-m0 -mthumb -Xlinker -Map=$(OUT_PATH)/$(APPLICATION)_m0.map -Wl,--gc-sections

all: $(APPLICATION)

$(APPLICATION):
	@for PROJECT in $(PROJECTS) ; do \
		echo "*** Building project $$PROJECT ***" ; \
		make all -C $$PROJECT ; \
		echo "" ; \
	done

clean:
	rm -rf $(OUT_PATH)/*.*
	rm -rf $(OBJ_PATH_M0)/*.*
	rm -rf $(OBJ_PATH_M4)/*.*

openocd:
	openocd -f ./config/cfg/lpc4337.cfg

download_m0:
	@echo ""
	@echo "*** Downloading $(APPLICATION)_m0.bin to EDU-CIAA-NXP... ***"
	openocd -f ./config/cfg/lpc4337.cfg -c "init" -c "halt 0" -c "flash write_image erase unlock $(OUT_PATH)/$(APPLICATION)_m0.bin 0x1B000000 bin" -c "reset run" -c "shutdown"

download_m4:
	@echo ""
	@echo "*** Downloading $(APPLICATION)_m4.bin to EDU-CIAA-NXP... ***"
	openocd -f ./config/cfg/lpc4337.cfg -c "init" -c "halt 0" -c "flash write_image erase unlock $(OUT_PATH)/$(APPLICATION)_m4.bin 0x1A000000 bin" -c "reset run" -c "shutdown"

download: $(APPLICATION)
	@make download_m0
	@make download_m4
	openocd -f ./config/cfg/lpc4337.cfg -c "init" -c "reset run" -c "shutdown"
	@echo ""
	@echo "*** Download done. ***"

erase:
	@echo ""
	@echo "*** Erasing flash memory bank A... ***"
	openocd -f ./config/cfg/lpc4337.cfg -c "init" -c "halt 0" -c "flash erase_sector 0 0 last" -c "shutdown"
	@echo ""
	@echo "*** Erasing flash memory bank B... ***"
	openocd -f ./config/cfg/lpc4337.cfg -c "init" -c "halt 0" -c "flash erase_sector 1 0 last" -c "shutdown"
	@echo ""
	@echo "*** Erase done. ***"
